# Code of Coding

The Code of Coding is a project management and relations mission-statement geared toward 
the promotion of meritocracy in the face of increasing hostility toward the 
principle within technical spaces due, in large part, to draconian and
paternalistic "Codes of Conduct" that have proliferated therein. It is the belief 
of the creators of this Code that these are poisonous to the communities that adopt them 
and perpetuate the false reality of wanton harassment and toxicity within them, and that the 
proliferators are often not acting with sincereity or without opportunism. Such policies often
serve as an excuse for blacklisting campaigns, creating persona non grata out of those who do not fall within 
'appropriate' ideological lines.  

Beyond the promotion of meritocracy, the Code has been written to avoid proscription 
whenever possible. Working professionals do not need written prohibitions of racism, sexism, or harassment
any more than they need them for murder or taking candy from children. 

It has, similarily, been written to avoid prescriptions whenever possible. Demanding
participants to act and communicate in the style that is most outwardly wholesome and avoiding confrontation
as a rule ultimately leads to indirect styles of communication where aggreeableness is their paramount duty, creating 
an environment that is hostile to meritocratic governance favour of protecting the sensitivites and 
neuroses of the perpetually offended. 


## Contribute
The Code welcomes discussion and contributions around it, including issues and pull requests. 

## Thanks
The creators of this project would like to give thanks to all those who believe in the 
spirit and pursual of the foundations of the Code, and those that have been damaged 
by those who work against them. Special thanks to the Code of Merit (http://code-of-merit.org) 
for being an ally in the promotion of meritocracy.